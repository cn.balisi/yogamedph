@extends('layouts.app')

@section('title')
YOGAMed
@endsection

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link  href="https://www.flaticon.com/authors/freepik">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Cuprum|Merriweather|Lato|Montserrat|Raleway&display=swap" rel="stylesheet">

<link rel="stylesheet" href="sweetalert2.min.css">
@section('content')
<div class="bc">
    

    <div class="box text-center">
        <img src="../images/lotus2 copy.png" width="120px" height="100px"alt="">
        <h1><span id="sign">Book a </h1>
        <h1><span id="yoga">FREE<span> <span id="for"></span><span id="mom"> CLASS </span> </h1>
        <div class="but">
            <a class="btn btn-box" href="#oc">RESERVE A SLOT NOW</a>
            </div>
    </div>

</div>
  

<div class="why-choose-yoga">
    	<div class="auto-container">
        	<!--Section Title-->
            <div class="sec-title centered">
            	<h2 id="RTC">Reasons to Choose Yoga</h2>
                <div class="desc-textlp">Our Main Factors</div>
                <div class="styled-line"></div>
            </div>
            
            <!--Features Outer-->
            <div class="features-outer" id="ima" style="background-image:url(images/yogay.png);">
            	<div class="row clearfix">
                	<!--Left Column-->
                    <div class="left-column col-md-6 col-sm-6 col-xs-12">
                    	<!--Feature Block-->
                        <article class="feature-block clearfix wow fadeInLeft animated" data-wow-duration="1500ms" data-wow-delay="0ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInLeft;">
                        	<div class="inner-box">
                            	<div class="icon-box"><span  class="fas fa-running"></span></div>
                            	<h3>Addition of Energy</h3>
                            	<div class="text">That this group would somehow form a family the way we all became the Brady Bunch.</div>
                            </div>
                        </article>
                        <!--Feature Block-->
                        <article class="feature-block clearfix wow fadeInLeft animated" data-wow-duration="1500ms" data-wow-delay="0ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInLeft;">
                        	<div class="inner-box">
                            	<div class="icon-box"><span class="fas fa-dumbbell"></span></div>
                            	<h3>Addition of Strength</h3>
                            	<div class="text">That this group would somehow form a family the way we all became the Brady Bunch.</div>
                            </div>
                        </article>
                        <!--Feature Block-->
                        <article class="feature-block clearfix wow fadeInLeft animated" data-wow-duration="1500ms" data-wow-delay="0ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInLeft;">
                        	<div class="inner-box">
                            	<div class="icon-box"><span class="fas fa-book-reader"></span></div>
                            	<h3>Free from Stress</h3>
                            	<div class="text">That this group would somehow form a family the way we all became the Brady Bunch.</div>
                            </div>
                        </article>
                    </div>
                    
                    <!--Right Column-->
                    <div class="right-column col-md-6 col-sm-6 col-xs-12">
                    	<!--Feature Block-->
                        <article class="feature-block clearfix wow fadeInRIght animated" data-wow-duration="1500ms" data-wow-delay="0ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms;">
                        	<div class="inner-box">
                            	<div class="icon-box"><img src="../images/bev.png" class="flaticon"></div>
                            	<h3>Relaxation and Refresh</h3>
                            	<div class="text">That this group would somehow form a family the way we all became the Brady Bunch.</div>
                            </div>
                        </article>
                        <!--Feature Block-->
                        <article class="feature-block clearfix wow fadeInRight animated" data-wow-duration="1500ms" data-wow-delay="0ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInRight;">
                        	<div class="inner-box">
                            	<div class="icon-box"><img src="../images/Butterfly.png" class="flaticon"></div>
                            	<h3>Beauty of Body</h3>
                            	<div class="text">That this group would somehow form a family the way we all became the Brady Bunch.</div>
                            </div>
                        </article>
                        <!--Feature Block-->
                        <article class="feature-block clearfix wow fadeInRight animated" data-wow-duration="1500ms" data-wow-delay="0ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInRight;">
                        	<div class="inner-box">
                            	<div class="icon-box"><img src="../images/Flower.png" class="flaticon"></div>
                            	<h3>Mind and Soul</h3>
                            	<div class="text">That this group would somehow form a family the way we all became the Brady Bunch.</div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
            
        </div>
</div>
            
      

   <div class="bcben">
        <div class="row">
        <div class="auto-container">
        	<!-- Section Title -->
             <div class="sec-title centered">
            	<h2 id="toy">TYPES OF YOGA</h2>
                <div class="desc-text"> There are kinds of Yoga and has different core that can choose and would fit for your needs.  </div>
                <div class="styled-line"></div>
            </div>
            
            <div class="row clearfix">
            	<!-- Yoga Nature Block  -->
                <div class="nature-block col-md-3 col-sm-6 col-xs-12 py-0">
                <div class="inner-box wow fadeInUp animated" data-wow-duration="1500ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 300ms; animation-name: fadeInUp;">
                    	<div class="icon-box"><img src="../images/yoga22.jpg" class="card-img-top"></div>
                        <h3>Basic or Hatha Yoga</h3>
                        <h5>For Beginners</h5>
                        <p class="text">Hatha Yoga is the perfect option for Yoga newbies!You’ll flow between poses, paying close attention to the rhythm of your breathing. 
                            It’s much more focused on finding that balance between body and breath than pushing you to the limits of your fitness and mobility.

                        </p>
                        <div class="styled-dots"></div>
                        
                       
                        <a href="#oc" class="theme-btn btn-style-one">Join our class!</a>
                    </div>	
                </div>
                
                <!-- Yoga Nature Block  -->
                <div class="nature-block col-md-3 col-sm-6 col-xs-12 py-0">
                	<div class="inner-box wow fadeInUp animated" data-wow-duration="1500ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 300ms; animation-name: fadeInUp;">
                    	<div class="icon-box"><img src="../images/ashtanga.jpg" class="card-img-top"></div>
                        <h3>Ashtanga Yoga</h3>
                        <h5>For Building Strength</h5>
                        <div class="text">Ashtanga Yoga practice includes the same 70 poses executed in a 90 to 120-minute practice. The series rarely changes, so it’s easier for your body to adjust to the movements.
                             The focus isn't the breathing but on the way our bodies move.</div>
                        <div class="styled-dots"></div>
                        <a href="#oc" class="theme-btn btn-style-one">Join our class!</a>
                    </div>
                </div>
                
                <!--Yoga Nature Block-->
                <div class="nature-block col-md-3 col-sm-6 col-xs-12 py-0">
                	<div class="inner-box wow fadeInUp animated" data-wow-duration="1500ms" data-wow-delay="600ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 600ms; animation-name: fadeInUp;">
                    	<div class="icon-box"><img src="../images/vinyasa.jpg" class="card-img-top"></div>
                        <h3>Vinyasa or Flow Yoga</h3>
                        <h5>For Weight Loss</h5>
                        <div class="text"> Vinyasa Yoga, or Flow Yoga, is a type of Yoga practice that involves continuous movement.
                             Instead of holding the poses (like in Ashtanga Yoga) to develop strength and endurance, the never-stopping Vinyasa Yoga focuses on cardiovascular endurance.</div>
                        <div class="styled-dots"></div>
                        <a href="#oc" class="theme-btn btn-style-one">Join our class!</a>
                    </div>
                </div>
                <!--Yoga Nature Block-->
                <div class="nature-block col-md-3 col-sm-6 col-xs-12 py-0">
                	<div class="inner-box wow fadeInUp animated" data-wow-duration="1500ms" data-wow-delay="600ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 600ms; animation-name: fadeInUp;">
                    	<div class="icon-box"><img src="../images/kundalini.jpg" class="card-img-top"></div>
                        <h3>Kundalini Yoga</h3>
                        <h5>For Stronger Mind and Body</h5>
                        <div class="text">The fact that Kundalini Yoga focuses more on the mind means it’s excellent for mothers who have a lot of stress to deal with at home.  
                            By focusing on the meditation, breath work, and relaxation exercises, you can have a few moments of peace in your day.</div>
                        <div class="styled-dots"></div>
                        <a href="#oc" class="theme-btn btn-style-one">Join our class!</a>
                    </div>
                </div>
                </div>
            </div>
        </div>
</div>
</div>
</div>
</div>  
                      
<div class="bcbchan">
    <div class="auto-container">
<div class="sec-title centered">
            	<h2 id="oc">Our Classes</h2>
                <div class="desc-text"> Join our classes to feel relaxing and rejuvenating experience for your mind and body. </div>
                <div class="styled-line"></div>
            </div>  
    <div class="row" id="products">
    
                	        	
                    </div>
                </div>   
                </div>
                </div>
    </div>
</div>
       

    <div class="team-section">
    	<div class="auto-container"> 
            <div class="sec-title centered">
            	<h2 id="lp">Learn Positions</h2>
                <div class="desc-textlp">Proper Steps to Yoga</div>
                <div class="styled-line"></div>
            </div>
            <div class="team-carousel owl-carousel owl-theme owl-loaded">      
                <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-2054px, 0px, 0px); transition: all 0.5s ease 0s; width: 5134.99px;">
                <div class="owl-item active" style="width: 322.333px; height:25%; margin-right: 20px;">

            <div class="team-member">
                <div class="inner-box"><img img alt="flower" class="inner-box"  src="images/yogay.png" ></div>
            </div>
            </div>

            <div class="owl-item active" style="width: 322.333px; margin-right: 20px;">

<div class="team-member">
    <div class="inner-box"><img alt="flower" class="inner-box" src="images/yoga43.jpg"></div>
    </div>
            </div>
    <div class="owl-item active" style="width: 322.333px; margin-right: 20px;">

<div class="team-member">
    <div class="inner-box"><img alt="flower" class="inner-box" src="images/yoga41.jpg"></div>
    </div>
            </div>
    <div class="owl-item active" style="width: 322.333px; margin-right: 20px;">

<div class="team-member">
    <div class="inner-box"><img alt="flower" class="inner-box" src="images/yoga40.jpg"></div>
    </div>
            </div>
    <div class="owl-item active" style="width: 322.333px; margin-right: 20px;">

<div class="team-member">
    <div class="inner-box"><img alt="flower" class="inner-box" src="images/yoga39.jpg"></div>
    </div>
</div>
    <div class="owl-item active" style="width: 322.333px; margin-right: 20px;">

<div class="team-member">
    <div class="inner-box"><img alt="flower" class="inner-box" src="images/yoga38.jpg"></div>
    </div>
</div>
    <div class="owl-item active" style="width: 322.333px; margin-right: 20px;">

<div class="team-member">
    <div class="inner-box"><img alt="flower" class="inner-box" src="images/yoga37.jpg"></div>
    </div>
</div>
    <div class="owl-item active" style="width: 322.333px; margin-right: 20px;">

<div class="team-member">
    <div class="inner-box"><img alt="flower" class="inner-box" src="images/yoga36.jpg"></div>
    </div>
</div>
    <div class="owl-item active" style="width: 322.333px; margin-right: 20px;">

<div class="team-member">
    <div class="inner-box"><img alt="flower" class="inner-box" src="images/yoga35.jpg"></div>
    </div>
</div>
    <div class="owl-item active" style="width: 322.333px; margin-right: 20px;">

<div class="team-member">
    <div class="inner-box"><img alt="flower" class="inner-box" src="images/yoga33.jpg"></div>
    </div>
</div>
    <div class="owl-item active" style="width: 322.333px; margin-right: 20px;">

<div class="team-member">
    <div class="inner-box"><img alt="flower" class="inner-box" src="images/yoga29.jpg"></div>
    </div>
</div>
    <div class="owl-item active" style="width: 322.333px; margin-right: 20px;">

<div class="team-member">
    <div class="inner-box"><img alt="flower" class="inner-box" src="images/yoga28.jpg"></div>
            </div>
            </div>
    <div class="owl-item active" style="width: 322.333px; margin-right: 20px;">

<div class="team-member">
    <div class="inner-box"><img alt="flower" class="inner-box" src="images/yoga50.jpg"></div>
            </div>
            </div>
            </div>          
</div>            


</div>


</div>
</div>
<section class="contact-info-section">
    	<div class="auto-container">
        	<!--Section Title-->
            <div class="sec-title3 centered">
            	<h2 id="cu">Contact us</h2>
                <div class="desc-text3">Keep in touch with us</div>
                <div class="styled-line"></div>
            </div>
            
            <div class="row clearfix">
            	<!--Info Block-->
                <div class="info-block col-md-4 col-sm-4 col-xs-12">
                	<div class="inner-box">
                    	<div class="icon-box"><span class="fas fa-map-marker-alt"></span></div>
                        <div class="desc-text">4343 P.Valdez St, Makati, Philippines</div>
                    </div>
                </div>
                <!--Info Block-->
                <div class="info-block col-md-4 col-sm-4 col-xs-12">
                	<div class="inner-box">
                    	<div class="icon-box"><span class="fas fa-phone"></span></div>
                        <div class="desc-text">+632-456-5678</div>
                    </div>
                </div>
                <!--Info Block-->
                <div class="info-block col-md-4 col-sm-4 col-xs-12">
                	<div class="inner-box">
                    	<div class="icon-box"><span class="fas fa-envelope"></span></div>
                        <div class="desc-text">cathn.balisi@gmail.com</div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
    <a class="scroll-to-top scroll-to-target"  href="/" style="display: block;"><span class="fa fa-angle-up"></span></a>
  
    <div class="col-md-6">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.5786164328533!2d121.02863174986418!3d14.566073189774528!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c9ab744d3ad7%3A0xeb2cc6252474a457!2s4343p+B.+Valdez%2C+Makati%2C+1210+Kalakhang+Maynila!5e0!3m2!1sen!2sph!4v1563430644883!5m2!1sen!2sph" width="1400" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
            
        

<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.css"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.transitions.css"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.transitions.min.css"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/AjaxLoader.gif"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
      
      <script src="js/bootstrap.js"></script>

            <script src="js/jquery.min.js"></script>
            <script src="js/owl.carousel.min.js"></script> 
            <script>
          
            $(".owl-carousel").owlCarousel({
                autoplay: true,
                margin: 10,
                loop: true,
                nav: true,
                navText: [
			'<span aria-label="' + 'Previous' + '">&#x2039; prev</span>',
			'<span aria-label="' + 'Next' + '">next &#x203a;</span>'
		],
                dots:true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:3
                    }
    }
                
            });              

            </script>		
           
         <script src="{{ asset('js/sweetalert2.min.js') }}"></script>
        

    <script type="text/javascript">
        fetch('https://enigmatic-brook-44120.herokuapp.com/availabilities/').then(function(response) {
            return response.json();
        })
        .then(function(data) {
            let availabilities = data.availabilities;
            availabilities.forEach(function(availability) {
                document.getElementById("products").innerHTML += `               
                <div class="nature-block2 col-md-4 py-0">
                	<div class="inner-box2 data-duration="1500ms" data-delay="0ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms;">
                        <div class="icon-box2"><h4 class="slots">${availability.seats}</h4><p>slots left</p></div>                   
                                <h4 class="card-title-name">${availability.name}</h4>
                                <p class="card-text-desc">${availability.description}</p>
                                <p class="card-text-days"><i class="far fa-calendar-alt"></i>  ${availability.days}</p>
                                <p class="card-text-time"><i class="far fa-clock"></i>  ${availability.time}</p>
                                <p class="card-text-price">1 hr: PhP ${availability.price}</p>
                                <button class="book-btn theme-btn btn-style-one" onclick="book()" id="${availability._id}">Book now!</button>                        
                                                   
                `

                                                                           
                    if(availability.isActive == false) {
                    document.getElementById(availability._id).disabled = true;
                    document.getElementById(availability._id).innerHTML = "Unavailable";
                } else {
                    document.getElementById(availability._id).disabled = false;
                }
            });

            //turn the book-btn class into an array
            let buttons = document.querySelectorAll('.book-btn');

            //loop through the buttons array to add an event listener and associate specific product id to each one
            buttons.forEach(function(button) {
                //add onclick event listener to every button
                button.addEventListener('click', function() {
                    let id = this.getAttribute('id')
                    if(localStorage.getItem('token')==null) {
                        window.location.replace("/");
                        Swal.fire({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Please Login!',
                        });
                    } else { 
                        window.location.replace(`/availabilities/${id}`);
                    }
                });
            })
        })
        .catch(function(err) {
            console.log(err);
        });

   
    </script>
        
        @endsection

<!-- Yoga Nature Block
                <div class="nature-block col-md-3 col-sm-6 col-xs-12">
                	<div class="inner-box wow fadeInUp animated" data-wow-duration="1500ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 300ms; animation-name: fadeInUp;">
                    	<div class="icon-box"><img src="../images/ashtanga.jpg" class="card-img-top"></div>
                        <h3>Science of Youthfulness</h3>
                        <div class="text">These days are all Happy and Free these days are all share them with me oh baby so join us here each week .</div>
                        <div class="styled-dots"></div>
                        <a href="#" class="theme-btn btn-style-one">Read More</a>
                    </div>
                </div>
                
                Yoga Nature Block
                <div class="nature-block col-md-3 col-sm-6 col-xs-12">
                	<div class="inner-box wow fadeInUp animated" data-wow-duration="1500ms" data-wow-delay="600ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 600ms; animation-name: fadeInUp;">
                    	<div class="icon-box"><img src="../images/kundalini.jpg" class="card-img-top"></div>
                        <h3>Science of Integrating body</h3>
                        <div class="text">These days are all Happy and Free these days are all share them with me oh baby so join us here each week .</div>
                        <div class="styled-dots"></div>
                        <a href="#" class="theme-btn btn-style-one">Read More</a>
                    </div>
                </div>
                 Yoga Nature Block
                 <div class="nature-block col-md-3 col-sm-6 col-xs-12">
                	<div class="inner-box wow fadeInUp animated" data-wow-duration="1500ms" data-wow-delay="600ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 600ms; animation-name: fadeInUp;">
                    	<div class="icon-box"><img src="../images/vinyasa.jpg" class="card-img-top"></div>
                        <h3>Science of Integrating body</h3>
                        <div class="text">These days are all Happy and Free these days are all share them with me oh baby so join us here each week .</div>
                        <div class="styled-dots"></div>
                        <a href="#" class="theme-btn btn-style-one">Read More</a>
                    </div>
                </div> -->