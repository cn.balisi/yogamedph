@extends('layouts.app')

@section('title')
Admin Dashboard
@endsection
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link  href="https://www.flaticon.com/authors/freepik">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Cuprum|Merriweather|Lato|Montserrat|Raleway&display=swap" rel="stylesheet">

<link rel="stylesheet" href="sweetalert2.min.css">


@section('content')

<section class="page-title" style="background-image:url(images/yoga5.jpg);">
        <div class="auto-container">
            <h1 id="sc">Schedule</h1>
            <div class="desc-text">More days to schedule slots.</div>
        </div>
    </section>
    <section class="page-info">
        <div class="auto-container clearfix">
           </div>
        </div>
    </section>
    

<div class="sec-title3 centered">
    <h2 id="cu">Want to Add New Schedule?</h2>
    <div>
    <button type="button" data-toggle="modal" data-target="#avail-modal" class="theme-btn btn-sched btn-style-one">Add New Sched</button>
    </div>
</div>
    

     
<div id="avail-modal" class="modal fade" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                	<div class="modal-header">
                        <h4 class="modal-title"><strong>Availability</strong></h4>
					</div>
					<div id="status"></div>

        <div class="card-body">
            <form id="addItem">         
        
                <div class="form-group">
                    <label for="name">Name of availability:</label>
                    <input type="text" name="name" id="name">
                </div>

                <div class="form-group">
                    <label for="description">Describe the availability briefly:</label>
                    <input type="text" name="description" id="description">
                </div>

                <div class="form-group">
                    <label for="days">Days available:</label>
                    <input type="text" name="days" id="days">
                </div>

                <div class="form-group">
                    <label for="time">Time slot:</label>
                    <input type="text" name="time" id="time">
                </div>

                <div class="form-group">
                    <label for="seats">How many seats are available?</label>
                    <input type="number" name="seats" id="seats">
                </div>

                <div class="form-group">
                    <label for="price">Price per seat</label>
                    <input type="number" name="price" id="price">
                </div>

                <div class="form-group">
                <div class="modal-footer">
                    	<button class="btn btn-outline-danger" data-dismiss="modal">Back</button>
                        <a id="addToCat" href="#availabilities" onclick="submit()">Add to catalogue</a>
					</div>
                   
                </div>

            </form>
					</div> 

                </div>
            </div>
        </div>
    
    <hr>

    <table class="table table-striped table-responsive">
        <thead>
            <tr>
                <th scope="col">Availability ID</th>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Days</th>
                <th scope="col">Time</th>
                <th scope="col">Seats</th>
                <th scope="col">Price</th>
                <th scope="col">isActive?</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>

        <tbody id="availabilities">
            
        </tbody>
        </table>

    
   
    <a class="scroll-to-top scroll-to-target"  href="#sc" style="display: block;"><span class="fa fa-angle-up"></span></a>

    <script src="{{ asset('js/scripts.js') }}"></script>
    <script type="text/javascript">

        fetch('https://enigmatic-brook-44120.herokuapp.com/availabilities/').then(function(response) {
            return response.json();
        })
        .then(function(data) {
            let availabilities = data.availabilities;
            availabilities.forEach(function(availability) {
                document.getElementById("availabilities").innerHTML += `
                <tr>
                    <td>${availability._id}</td>
                    <td>${availability.name}</td>
                    <td>${availability.description}</td>
                    <td>${availability.days}</td>
                    <td>${availability.time}</td>
                    <td>${availability.seats}</td>
                    <td>${availability.price}</td>
                    <td>${availability.isActive}</td>
                    <td>
                        <button class="btn btn-info upd-btn" id="${availability._id}">Update</button>
                        <button class="btn btn-danger del-btn" id="${availability._id}">Disable</button>
                        <button class="btn btn-success act-btn" id="${availability._id}">Enable</button>
                    </td>
                </tr>
                `
            });

            //turn the upd-btn class into an array
            let updButtons = document.querySelectorAll('.upd-btn');

            //turn the del-btn class into an array
            let delButtons = document.querySelectorAll('.del-btn');

            //turn the act-btn class into an array
            let actButtons = document.querySelectorAll('.act-btn');

            //loop through the updButtons array to add an event listener and associate specific product id to each one
            updButtons.forEach(function(button) {
                //add onclick event listener to every button
                button.addEventListener('click', function() {
                    let id = this.getAttribute('id')
                    window.location.replace(`/availabilities/update/${id}`);
                });
            })
            //loop through the delButtons array to add an event listener and associate specific product id to each one
            delButtons.forEach(function(button) {
                //add onclick event listener to every button
                button.addEventListener('click', function() {
                    let id = this.getAttribute('id')
                    fetch(`https://enigmatic-brook-44120.herokuapp.com/availabilities/${id}`, {
                        method: 'PUT', 
                        headers: {
                            "Access-Control-Request-Headers": "Content-Type, Access-Control-Request-Method, X-Requested-With, Authorization",
                            "Content-Type": "application/json",
                            "Access-Control-Request-Method": "PUT",
                            "X-Requested-With": "XMLHttpRequest",
                            "Authorization": "Bearer " + localStorage.getItem('token')
                        },
                        //instead of deleting availabilities, disable them
                        body: JSON.stringify({
                            "isActive": false
                        }),
                    })
                    .then(function(response) {
                        return response.json();
                    })
                    .then(function(data) {
                        window.alert(data.data.message);
                    })
                    .catch(function(err) {
                        console.log("Something went wrong!", err);
                    });
                });
            });
            //loop through the actButtons array to add an event listener and associate specific product id to each one
            actButtons.forEach(function(button) {
                //add onclick event listener to every button
                button.addEventListener('click', function() {
                    let id = this.getAttribute('id')
                    fetch(`https://enigmatic-brook-44120.herokuapp.com/availabilities/${id}`, {
                        method: 'PUT', 
                        headers: {
                            "Access-Control-Request-Headers": "Content-Type, Access-Control-Request-Method, X-Requested-With, Authorization",
                            "Content-Type": "application/json",
                            "Access-Control-Request-Method": "PUT",
                            "X-Requested-With": "XMLHttpRequest",
                            "Authorization": "Bearer " + localStorage.getItem('token')
                        },
                        //instead of deleting availabilities, disable them
                        body: JSON.stringify({
                            "isActive": true
                        }),
                    })
                    .then(function(response) {
                        return response.json();
                    })
                    .then(function(data) {
                        window.alert(data.data.message);
                    })
                    .catch(function(err) {
                        console.log("Something went wrong!", err);
                    });
                });
            });
        })
        .catch(function(err) {
            console.log(err);
        });

        function submit() {
            //select the form element
            const formElement = document.getElementById('addItem');
            //using FormData, the form input names and their corresponding values will be transformed to JSON format
            const formData = new FormData(formElement);
            //iterate through the formData and save each key-value pair to JSON
            let jsonObject = {};
            for (const [key, value] of formData.entries()) {
                jsonObject[key] = value;
            };
            console.log(jsonObject);

            //store all headers into a single variable
            let reqHeader = new Headers();
            reqHeader.append('Access-Control-Request-Headers', 'Content-Type, Access-Control-Request-Method, X-Requested-With, Authorization');
            reqHeader.append('Content-Type', 'application/json');
            reqHeader.append('Access-Control-Request-Method', 'POST');
            reqHeader.append('X-Requested-With', 'XMLHttpRequest');
            reqHeader.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

            //create optional init object for supplying options to the fetch request
            let initObject = {
                method: 'POST', headers: reqHeader, body: JSON.stringify(jsonObject),
            };
            
            //create a resource request object through the Request() constructor
            let clientReq = new Request('https://enigmatic-brook-44120.herokuapp.com/availabilities', initObject);

            //pass the request object as an argument for our fetch request
            fetch(clientReq)
                .then(function(response) {
                    return response.json();
                })
                .then(function(data) {
                    console.log(JSON.stringify(data));
                    document.getElementById('status').innerHTML = JSON.stringify(data.data.message);
                })
                .catch(function(err) {
                    console.log("Something went wrong!", err);
                });
        };
    </script>

        
            

            
        
@endsection