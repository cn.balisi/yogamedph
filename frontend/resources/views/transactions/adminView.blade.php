@extends('layouts.app')

@section('title')
Transactions History
@endsection

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link  href="https://www.flaticon.com/authors/freepik">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Cuprum|Merriweather|Lato|Montserrat|Raleway&display=swap" rel="stylesheet">

<link rel="stylesheet" href="sweetalert2.min.css">

@section('content')


<section class="page-title" style="background-image:url(images/yoga5.jpg);">
        <div class="auto-container">
            <h1 id="T">Transactions</h1>
            <div class="desc-text">More bookings.</div>
        </div>
    </section>
    <section class="page-info">
        <div class="auto-container clearfix">
           </div>
        </div>
    </section>
 
    <table class="table trans table-striped table-responsive">
        <thead>
            <tr>
                <th scope="col">Date</th>
                <th scope="col">Transaction ID</th>
                <th scope="col">Email</th>
                <th scope="col">Availability ID</th>
                <th scope="col">Seats Booked</th>
                <th scope="col">Total Amount</th>
                <th scope="col">Status</th>
            </tr>
        </thead>
        <tbody id="transactions"></tbody>
    </table>

    <script type="text/javascript">
        fetch('https://enigmatic-brook-44120.herokuapp.com/transactions/all', {
            method: "GET",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem('token')
            }
        })
        .then(function(response) {
            return response.json();
        })
        .then(function(data) {
            let transactions = data.data.transactions;
            transactions.forEach(function(transaction) {
                document.getElementById("transactions").innerHTML += `
                <tr>
                    <th scope="row">${transaction.date}</th>
                    <td>${transaction._id}</td>
                    <td>${transaction.ownerEmail}</td>
                    <td>${transaction.availabilityId}</td>
                    <td>${transaction.quantity}</td>
                    <td>${transaction.amount}</td>
                    <td>${transaction.status}</td>
                </tr>
                ` 
            });
        })
        .catch(function(err) {
            console.log(err);
        });
    </script>

        
            

            
        
@endsection