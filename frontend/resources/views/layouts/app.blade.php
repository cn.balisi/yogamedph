<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Playball&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css">  
	  <link rel="stylesheet" href="owlcarousel/owl.carousel.min.css">
	 <link rel="stylesheet" href="owlcarousel/owl.theme.default.min.css">
	 <link rel="stylesheet" href="{{asset('css/sweetalert2.min.css') }}">
	  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	  <link rel="icon" type="image/png" href="/images/lotus2.png">

  	
	<title>@yield('title', '')</title>
	<link rel="icon" alt class="titlelogo"type="images/png" href="../images/icon.png">
</head>
<body>
	
	 <nav class="navbar navbar-expand-lg fixed-top" id="navBar">
           
		   </nav>
		

					<div id="login-modal" class="modal fade" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                	<div class="modal-header">
                        <h4 class="modal-title"><strong>Login</strong></h4>
                    </div>

                    <div class="modal-body">
						<form id="myForm1">
                    	<div class="form-group row mb-2">
                    		<label for="login-email" class="col-4 col-form-label text-md-right">Email</label>
                    		<div class="col-8">
                    			<input id="email" class="form-control form-edit" type="email" name="email" value="" required>
                    		</div>
                    	</div>

                    	<div class="form-group row mb-2">
                    		<label for="register-password" class="col-4 col-form-label text-md-right">Password</label>
                    		<div class="col-8">
                    			<input id="password" class="form-control form-edit" type="password" name="password" value="">
                    		</div>
                    	</div>

                        <div class="form-group row mb-2">
                            <label class="col-4 col-form-label text-md-right" for="login-remember">Remember?</label>
                            <div class="col-8">
                                <select id="login-remember" class="form-control">
                                    <option value="no">No</option>
                                    <option value="yes">Yes</option>
                                </select>
                            </div>
						</div>
						<div class="modal-footer">
                    	<button class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                    	<button id="login-btn" type="button" class="btn btn-pink" onclick="login()">Login</button>
					</div>
					</form>
						
                    </div>

                    

                </div>
            </div>
		</div>
		
	
		
		<div id="register-modal" class="modal fade" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                	<div class="modal-header">
                        <h4 class="modal-title"><strong>Register</strong></h4>
					</div>
					<div id="status"></div>

                    <div class="modal-body">
					<form id="myForm2">
                    	<div class="form-group row mb-2">
							
                    		<label for="register-name" class="col-4 col-form-label text-md-right">Name</label>
                    		<div class="col-8">
                    			<input id="name" class="form-control form-edit" type="text" name="name" required>
                    			
                    		</div>
                    	</div>

                    	<div class="form-group row mb-2">
                    		<label for="register-email" class="col-4 col-form-label text-md-right">Email</label>
                    		<div class="col-8">
                    			<input id="reg-email" class="form-control form-edit" type="email" name="email" required>
                    			
                    		</div>
                    	</div>

                    	<div class="form-group row mb-2">
                    		<label for="password" class="col-4 col-form-label text-md-right">Password</label>
                    		<div class="col-8">
                    			<input id="pw1" class="form-control form-edit" type="password" name="password">
                    			
                    		</div>
                    	</div>

                    	<div class="form-group row mb-2">
                    		<label for="password2" class="col-4 col-form-label text-md-right">Confirm Password</label>
                    		<div class="col-8">
                    			<input id="pw2" class="form-control form-edit" type="password" name="password2" onblur="pwCheck()">
                    			
                    		</div>
						</div>
						<div class="modal-footer">
                    	<button class="btn btn-outline-pink" data-dismiss="modal">Cancel</button>
                    	<button id="btn" type="button" class="btn btn-pink" onclick="send()">Register</button>
					</div>
                                 
					</form>
					</div> 

                </div>
            </div>
        </div>


				
					
	@yield('content')
	<footer class="main-footer">
    	<!--Upper-->
        <div class="footer-upper">
        	<div class="auto-container">
                <div class="upper-container">
                    <!--Logo Image-->
                    <a href="/"><img class="logo-image" src="../images/lotus2 copy.png" alt="" width="150px" height="100px">
                    <div class="logo-image2"><span id="y">Y</span><span id="o">O</span><span id="y">GA</span><span id="m">Med </span></div></a>
                    
                    <div class="row clearfix">
                    	<!--Footer Widget-->
                        <div class="footer-column col-md-2 col-sm-6 col-xs-12">
                        	<div class="footer-widget work-hours">
                            	<h2>Working Hours</h2>
                                <ul>
                                	<li>Mon-Fri : 7am to 7pm</li>
                                    <li>Saturday : 7am to 4pm</li>
                                    <li>Sunday : 8am to 3pm</li>
                                </ul>
                            </div>
                        </div>
                        <!--Footer Widget-->
                        <div class="footer-column col-md-3 col-sm-6 col-xs-12">
                        	<div class="footer-widget links-widget">
                            	<h2>Our Services</h2>
                                <ul>
                                	<li><a href="/">About</a></li>
                                    <li><a href="#RTC">Pages</a></li>
                                    <li><a href="#oc">Classes</a></li>
                                    <li><a href="#cu">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        
                        <!--Footer Widget-->
                        <div class="footer-column col-md-3 col-sm-6 col-xs-12">
                        	<div class="footer-widget social-links-widget">
                            	<h2>Follow us on</h2>
                                <ul>
                                	<li><a href="https://www.facebook.com/apple.nitullano/"><span class="icon fab fa-facebook"></i></span> Facebook</a></li>
                                    <li><a href="#"><span class="icon fab fa-twitter"></span> Twitter</a></li>
                                    <li><a href="https://www.instagram.com/toyotailovedeals/"><span class="icon fab fa-instagram"></i></span> Instagram</a></li>
                                </ul>
                            </div>
                        </div>
                        
                        <!--Footer Widget-->
                        <div class="footer-column col-md-4 col-sm-6 col-xs-12">
                        	<div class="footer-widget newsletter-widget">
                            	<h2>Subscribe</h2>
                                <div class="newsletter-form">
                                	<form method="post" action="contact.html">
                                    	<div class="form-group">
                                        	<input type="email" name="useremail" value="" placeholder="E-Mail" required="">
                                            <button type="submit" class="theme-btn"><span class="flaticon-e-mail-envelope"></span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        
        <!--Upper-->
        <div class="footer-bottom">
        	<div class="auto-container">
            	<div class="copyright">© COPYRIGHTS 2019 YOGAMED. ALL RIGHTS RESERVED.      For Educational use only. </div>
            </div>
        </div>
        
    </footer>
    
    
	
		
	{{-- run external js once content has loaded --}}
	<script src="{{ asset('js/scripts.js') }}"></script>
	<script src="{{ asset('js/jquery.min.js') }}"></script>
	   <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
	   <script src="{{ asset('js/sweetalert2.min.js') }}"></script>

	   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.css"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.transitions.min.css"></script>
	 
	  
	  <script>
	  function login() {
            //select the form element
            const formElement = document.getElementById('myForm1');
            //using FormData, the form input names and their corresponding values will be transformed to JSON format
            const formData = new FormData(formElement);
            //iterate through the formData and save each key-value pair to JSON
            let jsonObject = {};
            for (const [key, value] of formData.entries()) {
                jsonObject[key] = value;
            };

            //store all headers into a single variable
            let reqHeader = new Headers();
            reqHeader.append('Access-Control-Request-Headers', 'Content-Type, Access-Control-Request-Method, X-Requested-With');
            reqHeader.append('Content-Type', 'application/json');
            reqHeader.append('Access-Control-Request-Method', 'POST');
            reqHeader.append('X-Requested-With', 'XMLHttpRequest');
            //create optional init object for supplying options to the fetch request
            let initObject = {
                method: 'POST', headers: reqHeader, body: JSON.stringify(jsonObject),
            };

            //create a resource request object through the Request() constructor
            let clientReq = new Request('https://enigmatic-brook-44120.herokuapp.com/auth/login', initObject);

            //pass the request object as an argument for our fetch request
            fetch(clientReq)
                //return the response received from API as a JSON upon resolving this promise
                .then(function(response) {
                    return response.json();
                })
                //pass the previously returned JSON response as an argument to the next promise
                .then(function(response) {
                    //save received token from API into a variable
                    let userToken = (response.data.token);
                    //save user email from API response into a variable
                    let userEmail = (response.data.user.email);
                    //save user ID from API response into a variable
                    let userId = (response.data.user._id);
                    //save isAdmin status from API response into a variable
                    let userIsAdmin = (response.data.user.isAdmin);
                    //store token variable in localStorage for use in subsequent requests
                    localStorage.setItem('token', userToken);
                    //store user email variable in localStorage
                    localStorage.setItem('email', userEmail);
                    //store userIsAdmin variable in localStorage
                    localStorage.setItem('isAdmin', userIsAdmin);
                    //store userId variable in localStorage
                    localStorage.setItem('userId', userId);
                    //check if logged in user is an admin
                    if(response.data.user.isAdmin == true) {
                        //redirect to admin dashboard
						window.location.replace("/admin");
						Swal.fire('Welcome back, Admin!');
                    } else {
                        //redirect back to catalogue
						window.location.replace("/");
						Swal.fire('Hello, Please Enjoy!');
                    };                    
                })

                //catch any exception and display below message in the console
                .catch(function(err) {
					console.log("Something went wrong!", err);
					Swal.fire(
						'Already registered?',
						'Please check details',
						'question'
				);
					document.querySelector("#email").value ="Please re-enter details.. ";
					document.querySelector("#password").value =" ";
					errors++;
					return false;
				});
        };
	</script>
	 <script>
        
        //disable button if passwords given don't match
        function pwCheck() {
            let pw1 = document.getElementById('pw1').value;
            let pw2 = document.getElementById('pw2').value;

            if(pw1!=pw2) {
                document.getElementById('btn').disabled = true;
                document.getElementById('status').innerHTML = "passwords don't match";
            } else {                
                document.getElementById('btn').disabled = false;
            }
        };

        function send() {
            //select the form element
            const formElement = document.getElementById('myForm2');
            //using FormData, the form input names and their corresponding values will be transformed to JSON format
            const formData = new FormData(formElement);
            //iterate through the formData and save each key-value pair to JSON
            let jsonObject = {};
            for (const [key, value] of formData.entries()) {
                jsonObject[key] = value;
            };

            //store all headers into a single variable
            let reqHeader = new Headers();
            reqHeader.append('Access-Control-Request-Headers', 'Content-Type, Access-Control-Request-Method, X-Requested-With');
            reqHeader.append('Content-Type', 'application/json');
            reqHeader.append('Access-Control-Request-Method', 'POST');
            reqHeader.append('X-Requested-With', 'XMLHttpRequest');

            //create optional init object for supplying options to the fetch request
            let initObject = {
                method: 'POST', headers: reqHeader, body: JSON.stringify(jsonObject),
            };
            
            //create a resource request object through the Request() constructor
            let clientReq = new Request('https://enigmatic-brook-44120.herokuapp.com/register', initObject);

            //pass the request object as an argument for our fetch request
            fetch(clientReq)
                .then(function(response) {
                    return response.json();
                })
                .then(function(data) {
                    console.log(JSON.stringify(data));
					document.getElementById('status').innerHTML = JSON.stringify(data.message);
					document.querySelector("#name").value = " ";
					document.querySelector("#reg-email").value = " ";
					document.querySelector("#pw1").value = " ";
					document.querySelector("#pw2").value = " ";

										
				}).then(function(response) {
					let error = 0;
					let userToken = 0;
				
					if(response.data.user.token == !null) {
                        
						alert('user already register');
					} else {
					Swal.fire({
					title: 'Your comment is a big help!',
					text: 'Get in touch with you soon...',
					imageUrl: "/images/ty.jpg",
					imageWidth: 400,
					imageHeight: 200,
					imageAlt: 'Custom image',
					animation: false
					});
				}
					
				})
				// 	let err = 0;
				// 	let userToken = 0; 

				// if(!err || !userToken) {
				// 	Swal.fire({
				// 	title: 'Your comment is a big help!',
				// 	text: 'Get in touch with you soon...',
				// 	imageUrl: "/images/ty.jpg",
				// 	imageWidth: 400,
				// 	imageHeight: 200,
				// 	imageAlt: 'Custom image',
				// 	animation: false
				// 	}):
				// 		}		else {
				// 					alert('already registered');
				// 				};
															
				// 				window.location.replace("/");
													
				// 	})
					.catch(function(err) {
					console.log("Something went wrong!", err);
					// Swal.fire(
					// 	'Not yet registered?',
					// 	'Please check details',
					// 	'question'
					// );
					// document.querySelector("#name").value = " ";
					// document.querySelector("#reg-email").value = " ";
					// document.querySelector("#pw1").value = " ";
					// errors++;
					
					});
									
				};
							
					
							
					
				</script>
			

			</body>
			</html>	
						
						
							