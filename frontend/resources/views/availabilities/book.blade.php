@extends('layouts.app')

@section('title')
View Class


@endsection
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Playball&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css">  
	  <link rel="stylesheet" href="owlcarousel/owl.carousel.min.css">
	 <link rel="stylesheet" href="owlcarousel/owl.theme.default.min.css">
	 <link rel="stylesheet" href="{{asset('css/sweetalert2.min.css') }}">
  	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
<link rel="stylesheet" type="text/css" href="css/sweetalert2.min.css">

@section('content')

<section class="page-title" style="background-image:url(images/yogatour.jpg);">
        <div class="auto-container">
            <h1 class="yc" id="sc">My Chosen Class</h1>
            <div class="desc-text">It is great to know your that we will be partner on your wellness.</div>
        </div>
    </section>
    <section class="page-info">
        <div class="auto-container clearfix">
           </div>
        </div>
    </section>
    
<div class="sec-title3 centered">
    <h2  id="cu">Time for Wellness</h2>

    <div class="container-fluid booke">
        <div class="classslot">
        <div class="page-header">
            <h3 id="productName"></h3>
        </div>
        <p id="description"></p>
        <p id="days"></p>
        <p id="time"></p>
        <p id="seats"></p>
        <p id="price"></p>
        </div>
        <form id="buy">
            
            <div class="form-group">
                <label for="quantity">How many slot would you like to book?</label>
                <input type="number" name="quantity" min="1" id="quantity">
            </div>

            <div class="form-group">
                 <a href="/" class="btn-dark btn">Back</a>
                 <a href="/#oc" class="btn btn-warning">More Sched</a>
                <button type="button" class="btn btn-success" onclick="book()">Book now</button>
                         
            </div> 
           
        </form>
    </div>
</div>
    <a class="scroll-to-top scroll-to-target"  href=".yc" style="display: block;"><span class="fa fa-angle-up"></span></a>



   
    	<script src="{{ asset('js/scripts.js') }}"></script>
	<script src="{{ asset('js/jquery.min.js') }}"></script>
	   <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
	   <script src="{{ asset('js/sweetalert2.min.js') }}"></script>

	   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        

<script type="text/javascript">
        //send a GET request using the availability ID as a wildcard to view specific product details
        fetch('https://enigmatic-brook-44120.herokuapp.com/availabilities/{{$id}}')
        .then(function(response) {
            return response.json();
        })
        .then(function(data) {
            //dynamically fill in product info from API's response
            document.getElementById("productName").innerHTML ="Class for: " + " " + data.availability.name;
            document.getElementById("description").innerHTML ="Design for: " + " " + data.availability.description;
            document.getElementById("days").innerHTML ="Day/Date: " + " " + data.availability.days;
            document.getElementById("time").innerHTML = "Time: " + " " + data.availability.time;
            document.getElementById("seats").innerHTML = "Available Slot: " + " " + data.availability.seats;
            document.getElementById("price").innerHTML = "Price per session:Php  " + " " + data.availability.price;
        })
       .catch(function(err) {
            console.log(err);
        });

       function book() {
            //get quantity from form
            const formElement = document.getElementById('buy');
            const formData = new FormData(formElement);
            let jsonObject = {};
            for (const [key, value] of formData.entries()) {
                jsonObject[key] = value;
            };
            //add ID of chosen booking to jsonObject
            jsonObject.id = "{{$id}}";
            //add user email to jsonObject
            jsonObject.email = localStorage.getItem('email');
            console.log(JSON.stringify(jsonObject));

            //store all headers into a single variable
            let reqHeader = new Headers();
            reqHeader.append('Access-Control-Request-Headers', 'Content-Type, Access-Control-Request-Method, X-Requested-With, Authorization');
            reqHeader.append('Content-Type', 'application/json');
            reqHeader.append('Access-Control-Request-Method', 'POST');
            reqHeader.append('X-Requested-With', 'XMLHttpRequest');
            reqHeader.append('Authorization', 'Bearer ' + localStorage.getItem('token'));

            //create optional init object for supplying options to the fetch request
            let initObject = {
                method: 'POST', headers: reqHeader, body: JSON.stringify(jsonObject),
            };

            //create a resource request object through the Request() constructor
            let clientReq = new Request('https://enigmatic-brook-44120.herokuapp.com/transactions/', initObject);

            //use above request object as the argument for our fetch request
            fetch(clientReq).then(function(response) {
                return response.json();
            })
            .then(function(response) {
                console.log(response);
                

                Swal.fire(
                    'Booked!',
                    'Please check transaction. See you in class!',
                    'success'
                    );
                    
            })
            
            .catch(function(err) {
                console.log("Something went wrong!", err);
            });

       };
    </script>




@endsection