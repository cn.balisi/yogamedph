//toggle navbar to reflect different options depending on logged in user
function checkUser() {
			let isAdmin = localStorage.getItem('isAdmin');
			if(isAdmin == "true") {
				document.getElementById("navBar").innerHTML = `
				<a class="navbar-brand" href="/">
				<img src="../images/lotus2 copy.png" width="30px" height="30px">
				<span id="y">Y</span><span id="o">O</span><span id="y">GA</span><span id="m">Med </span>
				</a>
				    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar"><span class="navbar-toggler-icon"></span>
				    </button>
				    <div class="collapse navbar-collapse" id="collapsibleNavbar">
						<ul class="navbar-nav ml-auto">
							<li class="nav-item ">
						        <a class="nav-link" href="/">Home</a>
							</li>
							<li class="nav-item ">
							<a class="nav-link" href="/admin">Schedule</a>
							</li>  
					        <li class="nav-item ">
					            <a class="nav-link" href="/admin/transactions">Transactions</a>
					        </li>
					        <li class="nav-item ">
					            <a class="nav-link" href="#" id="logout">Logout</a>
					        </li>    
					    </ul>
				    </div>
				`
			} else if(isAdmin == "false") {
				document.getElementById("navBar").innerHTML = `
				<a class="navbar-brand" href="/">
				<img src="../images/lotus2 copy.png" width="30px" height="30px">
				<span id="y">Y</span><span id="o">O</span><span id="y">GA</span><span id="m">Med </span>
				</a>

				    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar"><span class="navbar-toggler-icon"></span>
				    </button>
				    <div class="collapse navbar-collapse" id="collapsibleNavbar">
						<ul class="navbar-nav ml-auto">
					        <li class="nav-item">
						        <a class="nav-link" href="/">Home</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/#oc">Classes</a>
							</li>

							<li class="nav-item">
								<a class="nav-link" href="/#RTC">Benefits</a>
							</li>
					        <li class="nav-item">
						        <a class="nav-link" href="/transactions/${localStorage.getItem('userId')}">My Transactions</a>
					        </li>
					        <li class="nav-item">
					            <a class="nav-link" href="#" id="logout">Logout</a>
					        </li>    
					    </ul>
				    </div>
				`
			} else {
				document.getElementById("navBar").innerHTML = `
					<a class="navbar-brand" href="/">
					<img src="../images/lotus2 copy.png" width="30px" height="30px">
					<span id="y">Y</span><span id="o">O</span><span id="y">GA</span><span id="m">Med </span>
					</a>
				    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar"><span class="navbar-toggler-icon"></span>
				    </button>
				    <div class="collapse navbar-collapse" id="collapsibleNavbar">
						<ul class="navbar-nav ml-auto">
					        <li class="nav-item">
						        <a class="nav-link" href="/">Home</a>
							</li>
							
							<li class="nav-item mr-auto">
								<a class="nav-link" href="#oc">Classes</a>
							</li>

							<li class="nav-item mr-auto">
								<a class="nav-link" href="#RTC">Benefits</a>
							</li>

					        <li class="nav-item mr-auto">
							<button type="button" data-toggle="modal" data-target="#login-modal" class="btn nav-link btn-login">Login</button>
							</li>  
							<li class="nav-item mr-auto">
                            <button type="button" data-toggle="modal" data-target="#register-modal" class="btn nav-link btn-reg">Register</button>
                            </li>  
					    </ul>
					</div>
					
				`
			}
		};


		

checkUser();

function logout() {

	fetch('https://enigmatic-brook-44120.herokuapp.com/auth/logout', {
            method: "GET",
            headers: {
                "Authorization" : "Bearer " + localStorage.getItem('token')
            }
        })
		.then(function(response) {
            return response.json();
        })
        .then(function(data) {
            localStorage.clear();
            window.location.replace("/");
        })
        .catch(function(err) {
            console.log(err);
        });
};

//if logout button exists, assign an onclick event for logging out
let logoutButton = document.getElementById("logout");
if(logoutButton) {
	logoutButton.addEventListener("click", logout);
}

